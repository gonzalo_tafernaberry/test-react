import React from "react";
import { authEndpoint, clientId, redirectUri } from "./config";
import axios from "axios";
import hash from "./hash";
import Player from "./Player";
import logo from './logo.svg';
import './App.css';


class App extends React.Component {
  constructor() {
    super();
    this.state = {
      token: null,
      data: null
    };
    this.getCurrentlyPlaying = this.getCurrentlyPlaying.bind(this);
    this.getCookie = this.getCookie.bind(this);
  }

  componentDidMount() {
    // Set token
    let _token = hash.access_token;

    if (_token) {
      // Set token
      this.setState({
        token: _token
      });
      this.getCurrentlyPlaying(_token);
    }
    //console.log("componentDidMount");
  }

  sendTrack(track) {
    document.cookie = "trackUser=" + track;
    window.location.replace(authEndpoint+"?client_id=" + clientId + "&redirect_uri=" + redirectUri + "&response_type=token&show_dialog=true","_self"); 
  }

  getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length === 2) return parts.pop().split(";").shift();
  }

  getCurrentlyPlaying(token) {
    //console.log("getCurrentlyPlaying");
    var trackUserCookie = this.getCookie("trackUser");
    //console.log(trackUserCookie);
    const api = "https://api.spotify.com/v1/search?q="+trackUserCookie+"&type=track&limit=1";
    axios.get(api, { headers: {"Authorization" : "Bearer " + token} })
        .then(res => {
          console.log(res.data.tracks.items[0]);
          this.setState({
            data: "https://open.spotify.com/embed/track/"+res.data.tracks.items[0].id
          });
          document.cookie = 'trackUser=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    });
  }
  

  renderPlayer() {
    return (
      <Player 
        data={this.state.data}
      />
    );
  }

  render() {

    return (
      
        <header className="App-header">
          <a
            href="/"
          >      
            <img src={logo} className="App-logo" alt="logo" />
          </a>
          {!this.state.token && (
            <div id="parent">
              <div className="grid">
                <a
                  onClick={() => this.sendTrack("Pearl+Jam")}
                  className="btn btn--loginApp-link"
                  href="/#"
                >
                  Pearl Jam
                </a>
              </div>
              <div className="grid">
                <a
                  onClick={() => this.sendTrack("Nirvana")}
                  className="btn btn--loginApp-link"
                  href="/#"
                >
                  Nirvana
                </a>
              </div>
            </div>
          )}          
        
        {this.state.token && (
            this.renderPlayer()
          )}
        </header>
    );
  }
}



export default App;
